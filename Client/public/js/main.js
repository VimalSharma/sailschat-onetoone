
var app = angular.module('ChatApp', ['ngRoute','checklist-model']);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "partials/login.html", controller: "LoginCtrl"})
    .when("/home", {templateUrl: "partials/home.html", controller: "HomeCtrl"})
    // Pages
    .otherwise("/404", {templateUrl: "partials/404.html", controller: "PageCtrl"});
}]);




app.controller('LoginCtrl', function ( $scope, $rootScope, $location, $http ) {
    $scope.setName = function() {
          $rootScope.username=$scope.username;
          $location.path("home");
    };
});



app.controller('HomeCtrl', function ($scope, $rootScope, $location, $http) {
  var IndividualId;
  var IsIndividual=false;
  $scope.users=[];
  $scope.individualusers=[];
  $scope.username;
  $scope.userid=Math.floor((Math.random() * 100) + 1);
  $scope.userrole;

  $scope.templateuser = {
    users: []
  };

  $scope.chatToIndividual = function(userID) {
    if(userID==1000){
      $("#individualChatDiv").empty();
      IsIndividual=false;
    }
    else{
      IndividualId=userID;
      if($scope.individualusers.indexOf(userID)<0){
        $scope.individualusers.push(userID);
      }
      IsIndividual=true;
      //$('#individualChatDiv').append("<div class='chag_avi' id="+userID+"><w-div id='gdrgf2l' class='ghkwmg0 g-1fe8fc g42iztb g-839exn gnbi1r3'><div id='dropdown' class='g-jmctzy g-yv3nhf gbdspmq' style='bottom: 326px;'><a href='javascript:void(0)' class='gt60d9b g-t5fzih g7vlg7k'><span class='g-bsn14n'></span> <!-- <span class='g441ubz'></span>  --></a> <div class='g5gm2yb'> <div class='gusnb58'><div id='message_div' class='g-80khsx g-5t2u5g content mCustomScrollbar'><p class='tag_style'><div id='repeat-message"+userID+"' class='repeat-message'></div></div><div class='gixitel'><form  class='gixitel'><div class='g-lqcqgf'><div class='gojhumm'><label class='g-4nfvgl ' for='gscwinput_663_1'> Message </label><div class='gaxobu1 g-1s0n8'><input type='text' maxlength='1000' class='g3n9tjw g-pf3d4f input' id='txtmessage"+userID+"' ></div></div></div><div class='g-34vs0r'><div class='g-w9ftwc'><button class='grrmvdd ' type='button' ng-click='SendMessage("+userID+")'> <span class='g-ch3r3w'>Send</span> <span class='gyy6mbi'></span></button></div></div></form></div></div></div></div></w-div></div>");
    }
  }

    var FADE_TIME = 150; // ms
    var TYPING_TIMER_LENGTH = 400; // ms
    var COLORS = ['#e21400', '#91580f', '#f8a700', '#f78b00','#58dc00', '#287b00', '#a8f07a', '#4ae8c4','#3b88eb', '#3824aa', '#a700ff', '#d300e7'];

    // Initialize variables
    var $window = $(window);
    var $messages = $('.messages'); // Messages area
    var $inputMessage = $('.inputMessage'); // Input message input box
    var $userArea = $('#userArea');
    var $templateArea = $('#templates');
    var $input = $('.input');
    

    var $loginPage = $('.login.page'); // The login page
    var $chatPage = $('.chat.page'); // The chatroom page

    // Prompt for setting a username
    
    var currentUserId;
    var connected = false;
    var typing = false;
    var lastTypingTime;
    // var $currentInput = $usernameInput.focus();

    //var socket = io();
    var socket = io.connect('http://localhost:3002');

    $scope.SendIndividualMessage = function(id) {
      if ($scope.username) {
            sendMessage(id);
            socket.emit('stop typing');
            typing = false;
      } else {
            setUsername();
      }
    }

    var selectedTemplate="Gender Template";
    $scope.templates= [{id: 1, Description: "Gender Template", Name: "Gender Template",selected: false},
                     {id: 2, Description: "Template 1", Name: "Template 1",selected: false},
                     {id: 3, Description: "Template 2", Name: "Template 2",selected: false}
                    ];
    $scope.disableClass=false;
    $scope.ChangeTemplate=function(arg)
    {
        angular.forEach($scope.templates, function(temp) {
          if(temp.id!==arg.id){
            temp.selected = false;
          }
        });
        if(arg.selected){
          selectedTemplate=arg.Name;
          $scope.disableClass = true ;
        }else{
          $scope.disableClass = false ;
        }
    }

    $scope.SendTemplateMessage = function() {
      console.log($scope.templateuser);
      var message = cleanInput(selectedTemplate);
      if (message && connected) {
        angular.forEach($scope.templateuser.users, function(user) {
          addChatMessage({
            username: $scope.username,
            message: message,
            userid:user
          });
          var data={
            message:message,
            type:message.replace(" ", ""),
            userid:user
          };
          // tell server to execute 'new message' and send along one parameter
          socket.emit('new message', data);
        });
    }
  }



    function addParticipantsMessage (data,LeftMessage) {
      var message = '';
      if(data.role=="Admin" || $scope.userrole=="Admin"){
        if (data.numUsers === 1) {
          message += "there's 1 participant";
        } else {
          message += "there are " + data.numUsers + " participants";
        }
      }
      log(message);
    }

    $scope.setUsername = function() {
        if($rootScope.username){
          $scope.username = cleanInput($rootScope.username.trim());
          // If the username is valid
          if ($scope.username) {
            var user={username:$scope.username,userid:$scope.userid};
            $scope.userrole=$scope.username=="Avi"?$scope.userrole="Admin":$scope.userrole="User";
            user.role=$scope.userrole;      
            socket.emit('add user', user);
          }
        }else{
          $location.path("");
        }
    }

    $scope.Message = function() {
        if($rootScope.username){
          sendMessage();
          socket.emit('stop typing');
          typing = false;
        }else{
          $location.path("");
        }
    }

    // Sends a chat message
    function sendMessage (id) {
      var message;
      var ToUsers=id?id:"all";
      if(!id){
         message= $scope.txtMessage;
      }
      else{
        message = $("#txtmessage"+id).val();
        $("#txtmessage"+id).val("");
      }
      // Prevent markup from being injected into the message
      message = cleanInput(message);
      // if there is a non-empty message and a socket connection
      if (message && connected) {
        $inputMessage.val('');
        addChatMessage({
          username: $scope.username,
          message: message,
          userid:id
        });
        var data={
          message:message,
          userid:ToUsers,
          type:"plain"
        };
        // tell server to execute 'new message' and send along one parameter
        socket.emit('new message', data);
      }
    }

    // Log a message
    function log (message, options) {
      var $el = $('<li>').addClass('log').text(message);
      addMessageElement($el, options);
    }

    // Adds the visual chat message to the message list
    function addChatMessage (data, options) {
      // Don't fade the message in if there is an 'X was typing'
      var $typingMessages = getTypingMessages(data);
      options = options || {};
      if ($typingMessages.length !== 0) {
        options.fade = false;
        $typingMessages.remove();
      }

      var $usernameDiv = $('<span class="username"/>')
        .text(data.username)
        .css('color', getUsernameColor(data.username));
      var $messageBodyDiv = $('<span class="messageBody">')
        .text(data.message);

      var typingClass = data.typing ? 'typing' : '';
      var $messageDiv = $('<li class="message"/>')
        .data('username', data.username)
        .addClass(typingClass)
        .append($usernameDiv, $messageBodyDiv);

      addMessageElement($messageDiv, options,data.userid);
    }

    // Adds the visual chat typing message
    function addChatTyping (data) {
      data.typing = true;
      data.message = 'is typing';
      addChatMessage(data);
    }

    // Removes the visual chat typing message
    function removeChatTyping (data) {
      getTypingMessages(data).fadeOut(function () {
        $(this).remove();
      });
    }

    // Adds a message element to the messages and scrolls to the bottom
    // el - The element to add as a message
    // options.fade - If the element should fade-in (default = true)
    // options.prepend - If the element should prepend
    //   all other messages (default = false)
    function addMessageElement (el, options,id) {
      var $el = $(el);

      // Setup default options
      if (!options) {
        options = {};
      }
      if (typeof options.fade === 'undefined') {
        options.fade = true;
      }
      if (typeof options.prepend === 'undefined') {
        options.prepend = false;
      }

      // Apply options
      if (options.fade) {
        $el.hide().fadeIn(FADE_TIME);
      }

      var myElem = document.getElementById("repeat-message"+id);
      if(id && IsIndividual && myElem !=null){
        if (options.prepend) {
          $("#repeat-message"+id).prepend($el);
        } else {
          $("#repeat-message"+id).append($el);
        }
      }else{
        if (options.prepend) {
          $messages.prepend($el);
        } else {
          $messages.append($el);
        }
      }
      $messages[0].scrollTop = $messages[0].scrollHeight;
    }

    // Prevents input from having injected markup
    function cleanInput (input) {
      return $('<div/>').text(input).text();
    }

    // Updates the typing event
    function updateTyping () {
      if (connected) {
        if (!typing) {
          typing = true;
          socket.emit('typing');
        }
        lastTypingTime = (new Date()).getTime();

        setTimeout(function () {
          var typingTimer = (new Date()).getTime();
          var timeDiff = typingTimer - lastTypingTime;
          if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
            socket.emit('stop typing');
            typing = false;
          }
        }, TYPING_TIMER_LENGTH);
      }
    }

    // Gets the 'X is typing' messages of a user
    function getTypingMessages (data) {
      return $('.typing.message').filter(function (i) {
        return $(this).data('username') === data.username;
      });
    }

    // Gets the color of a username through our hash function
    function getUsernameColor (username) {
      // Compute hash code
      var hash = 7;
      for (var i = 0; i < username.length; i++) {
         hash = username.charCodeAt(i) + (hash << 5) - hash;
      }
      // Calculate color
      var index = Math.abs(hash % COLORS.length);
      return COLORS[index];
    }

    // Keyboard events

    // $window.keydown(function (event) {
    //   // Auto-focus the current input when a key is typed
    //   if (!(event.ctrlKey || event.metaKey || event.altKey)) {
    //     $currentInput.focus();
    //   }
    //   // When the client hits ENTER on their keyboard
    //   if (event.which === 13) {
    //     if (username) {
    //       sendMessage();
    //       socket.emit('stop typing');
    //       typing = false;
    //     } else {
    //       setUsername();
    //     }
    //   }
    // });

    $('.input').on('input', function() {
      updateTyping();
    });

    // Click events

    // Focus input when clicking anywhere on login page
    // $loginPage.click(function () {
    //   $currentInput.focus();
    // });

    // Focus input when clicking on the message input's border
    // $inputMessage.click(function () {
    //   $inputMessage.focus();
    // });

    // Socket events

    // Whenever the server emits 'login', log the login message
    socket.on('login', function (data) {
      connected = true;
      // Display the welcome message
      var message = "Welcome to Socket.IO Chat – ";
      log(message, {
        prepend: true
      });
      addParticipantsMessage(data);
    });

    // Whenever the server emits 'new message', update the chat body
    socket.on('new message', function (data) {
      console.log(data,$scope.userid);
      if(data.touser=="all" &&  data.role=="Admin"){
        addChatMessage(data);
      }
      else if($scope.userid==data.touser){
        addChatMessage(data);
      }
      else if($scope.userrole=="Admin"){
        addChatMessage(data);
      }
    });

    // Whenever the server emits 'user joined', log it in the chat body
    socket.on('user joined', function (data) {
      if(data.role=="Admin" || $scope.userrole=="Admin"){
        log(data.username + ' joined');
        addParticipantsMessage(data);
        $scope.users=data.users;
        $scope.$apply();
      }  
    });

    // Whenever the server emits 'user left', log it in the chat body
    socket.on('user left', function (data) {
      if(data.role=="Admin" || $scope.userrole=="Admin"){
        log(data.username + ' left');
        if(data.role=="Admin"){
          $( "#"+data.userid ).remove();
        }
        console.log("<<<<<<<",data)
        $scope.users=data.users;
        $scope.$apply();
        addParticipantsMessage(data,"LeftMessage");
        removeChatTyping(data);
      }    
    });

    // Whenever the server emits 'typing', show the typing message
    socket.on('typing', function (data) {
      if(data.role=="Admin"){
        addChatTyping(data);
      }
      else if($scope.userrole=="Admin"){
        addChatTyping(data);
      }
      
    });

    // Whenever the server emits 'stop typing', kill the typing message
    socket.on('stop typing', function (data) {
      removeChatTyping(data);
    });

});