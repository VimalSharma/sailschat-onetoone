﻿var env = process.env.NODE_ENV || 'development';

var config = {
    port: 3002,
    db: 'mongodb://localhost/ChatDB',
    host: '0.0.0.0'
};

if (env === "bluwemix") {
    var conf = JSON.parse(process.env.VCAP_SERVICES);
    config.db = conf["mongodb-2.2"][0].credentials.url;
    config.port = process.env.VCAP_APP_PORT || 80;
    config.host = process.env.VCAP_APP_HOST || 'localhost';
}
module.exports = config;
