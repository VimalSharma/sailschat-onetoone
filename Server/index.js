var express = require('express'), 
mongoose = require('mongoose'), 
fs = require('fs'), 
http = require('http'),
config = require('./config/config'), 
root = __dirname, 
app = express(), 
server = null;
var lessMiddleware = require('less-middleware');
var path=require('path');


// Configuration 
require('./config/db')(config);

var modelsPath = __dirname + '/server/models';
console.log(">>>>>>>>>",modelsPath);
fs.readdirSync(modelsPath).forEach(function (file) {
  if (file.indexOf('.js') >= 0) {
    require(modelsPath + '/' + file);
  }
});

var socket = require('./server/libs/socket.js');



require('./config/express')(app, config);

var server = http.createServer(app);
var io = require('socket.io')(server).listen(server);

io.sockets.on('connection', socket);
server.listen(config.port, config.host);
console.log('App started on port ' + config.port);



