﻿
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Message = new Schema({
  Sender : String,
  Message:String,
  Receivers : String,
  DeletedBy: String,
  SentDate: String,
  }, {
  versionKey: false,
  
  toJSON: {
    virtuals: true,
    transform: function(doc, ret, options) {
      ret.id = ret._id.toHexString();
      delete ret._id;
    }
  },
  
  toObject: {
    virtuals: true
  }
});

mongoose.model('Message', Message);