﻿var mongoose = require('mongoose');
var Message = mongoose.model('Message');
var async = require('async');
var Response = require('../libs/Response');
var E = require('../libs/Error');


exports.createMsg = function (data,callback) {
  if (Object.keys(data).length < 1) {
    // Empty body.
    callback(true,new E.EmptyBodyError());
  }else{
  	var message = new Message(data);
	  message.save(function(err, msg) {
	    if (err) callback(true,err);
	    return callback(false,msg);
	  });
  }
}

// module.exports.updateMsg = function updateMsg(req, res, next) {
//   var body = req.body;
//   var id = req.params.id;
//   Paystub.findOne({_id: id}, function findMsg(err, paystub) {
//     if (err) return next(err);
//     if (!paystub) return next(new E.RecordNotFoundError());
//     paystub.set(body);
//     paystub.save(function savePaystub(err, ok) {
//       if (err) return next(err);
//       return res.json(new Response(false, ok));
//     });
//   });
// }

// exports.messages = function (req, res) {
//   // Assume, we already know the companyId (i.e find the company of loggedin user)
//   var query = req.query;
//   var companyId = query.companyId,
//       limit = query.limit || 25;
//       skip = query.skip || 0;

//       Paystub
//         .find({'company.id': companyId})
//         .skip(skip)
//         .limit(limit)
//         .exec(function(err, paystubs) {
//           if (err) return next(err);
//           return res.json(new Response(false, paystubs));
//         });
// }



// exports.getmessageByuserId = function(req, res) {
//     if (req.body && Object.keys(req.body).length > 0) {
//         employee.find({
//             _id: req.body.empid,
//             isactive: true
//         }, function(err, employees) {
//             if (!err && employees != null) {
//                 var employee = employees;
//                 res.json({
//                     "status": true,
//                     "employee": employee
//                 });
//             }
//         })
//     } else {
//         res.json({
//             "status": false,
//             "code": 204,
//             "error": "Empty Body Request"
//         });
//     }
// }
