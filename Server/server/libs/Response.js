(function () {
    'use strict';

    var Response = function Response(isErr, data, opts) {
        opts = opts || {};
        var response = {
            ok: true,
            meta: {},
            results: {}
        };

        if (opts.monitor) {
          response.meta = opts.monitor;
        }

        // Put the timestamp in meta.
        response.meta.timestamp = (new Date()).toString();

        // if it's an error.
        if (isErr == true) {
            response.ok = false;  // no ok.
            response.errors = [];   // Initialize the key

            // Check if `data` is an instanceOf `Error`
            if ((data instanceof Error) === true) {
                if (data.name == 'ValidationError') {
                    // Mongoose error validation.
                    delete data.stack;
                    Object.keys(data.errors).forEach(function (key) {
                        var e = data.errors[key];
                        response.errors.push({
                            name: e.name,
                            message: String(e.message).replace('Path', 'Field'),
                            path: e.path
                        });
                    });

                    return response;
                }

                var _scope = {
                    name: data.name,
                    message: data.message
                };

                if (data._fields instanceof Array) {
                  _scope.fields = data.fields;
                }

                // it's General `Error`.
                response.errors.push(_scope);

                return response;
            }

            // if not `error`, check if it's array of errors
            if (data instanceof Array) {
                response.errors = data;
                return response;
            }

            if (data instanceof Object) {
                response.errors = [].push(data);
                return response;
            }
        }

        // Data on listing call
        if (data instanceof Array) {
            var total = opts.total || null; // if total is 0, please count the array.
            total = total || data.length; // total or calculate.
            response.total = total;
            response.results = [].concat(data);
            return response;
        }

        if (data instanceof Object) {
            response.results = data;
            return response;
        }

        // Its string, int, double etc.
        response.results = data;

        return response;
    };

    module.exports = Response;
}());
