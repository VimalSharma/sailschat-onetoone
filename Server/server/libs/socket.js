var Msg = require('../service/MessageService');


module.exports = function(socket) {
    var numUsers = 0;
    var users=[];
    var addedUser = false;

    console.log("::::::::::::Connected:::::::::::::");

    socket.on('new message', function (data) {

        console.log("::::::::::::Connected:::::::::::::",data);
        var data={Sender:socket.data.userid,Message:data.message,Receivers:data.userid,DeletedBy:"",SentDate:""};
        Msg.createMsg(data, function(err, result) {
          console.log(err, result);
          // we tell the client to execute 'new message'
          socket.broadcast.emit('new message', {
            username: socket.data.username,
            userid:socket.data.userid,
            role:socket.data.role,
            message: data.message,
            touser:data.userid,
            type:data.type
          });
        });
    });

   // when the client emits 'add user', this listens and executes
    socket.on('add user', function (data) {
        console.log(":::::User  Name::::  ",data);
        users.push(data);
        if (addedUser) return;

        // we store the username in the socket session for this client
        socket.data = data;
        ++numUsers;
        addedUser = true;
        socket.emit('login', {
          numUsers: numUsers
        });
        // echo globally (all clients) that a person has connected
        socket.broadcast.emit('user joined', {
          username: socket.data.username,
          userid:socket.data.userid,
          role:socket.data.role,
          numUsers: numUsers,
          users:users
        });
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', function () {
    socket.broadcast.emit('typing', {
      username: socket.data.username,
      userid:socket.data.userid,
      role:socket.data.role,
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', function () {
    socket.broadcast.emit('stop typing', {
      username: socket.data.username,
      userid:socket.data.userid,
      role:socket.data.role,
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', function () {
    if (addedUser) {
      --numUsers;
        users = users.filter(function (el) {return el.userid !== socket.data.userid;});
      // echo globally that this client has left
      socket.broadcast.emit('user left', {
        username: socket.data.username,
        userid:socket.data.userid,
        role:socket.data.role,
        numUsers: numUsers,
        users:users
      });
    }
  });
};